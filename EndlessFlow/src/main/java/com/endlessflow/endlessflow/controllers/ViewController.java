package com.endlessflow.endlessflow.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
//    @GetMapping("/")
//    public String index() {
//        return "index";
//    }
    @GetMapping("/")
    public String startPage() {
        return "preAuthStartPage";
    }
}
