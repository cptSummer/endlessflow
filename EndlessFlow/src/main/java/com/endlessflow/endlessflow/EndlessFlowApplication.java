package com.endlessflow.endlessflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EndlessFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(EndlessFlowApplication.class, args);
    }

}
