package com.endlessflow.endlessflow.security;


import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        String[] staticResources  =  {
                "/css/**",
                "/images/**",
                "/fonts/**",
                "/scripts/**",
        };

        String[] htmlResources ={
                "/index","index.html",
                "/preAuthStartPage","preAuthStartPage.html"

        };

        http.authorizeHttpRequests(authorize -> authorize.requestMatchers("/").permitAll()
                        .requestMatchers(staticResources).permitAll()
                        .requestMatchers(htmlResources).permitAll()
                        .requestMatchers("/admins/**").hasAuthority("admins")
                        .requestMatchers("/users/**").hasAuthority("users")
                        .anyRequest().authenticated())
                .formLogin().and()
                .csrf().disable();
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
